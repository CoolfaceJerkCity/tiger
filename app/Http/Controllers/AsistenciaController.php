<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AsistenciaController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'practicante']);
    }

    public function postRequest(Request $request): \Illuminate\Http\JsonResponse
    {
        $action = $request->get('action');
        try {
            $id = auth()->user()->practicante->id;

            if($action == 'list'){
                $data = DB::select("call sp_consulta_asistencia(?)", [$id]);
            }
            elseif($action == 'input'){
                $data = DB::select("call sp_marcar_entrada(?)", [$id]);
            }
            elseif($action == 'output'){
                $data = DB::select("call sp_marcar_salida(?)", [$id]);
            }
            else{
                $data = ['error'=>'No se ha ingresado ninguna acción'];
            }
        }catch (Exception $e) {
            $data = ['error'=> $e->getMessage()];
        }
        return response()->json(['data'=>$data]);
    }
}
