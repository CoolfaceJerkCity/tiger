<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PracticanteController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'practicante']);
    }

    public function assist()
    {
        return view('practicante.assist');
    }

    public function excuse()
    {
        return view('practicante.excuse');
    }
}
