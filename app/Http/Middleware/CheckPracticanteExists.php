<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckPracticanteExists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(!auth()->user()->practicante){
            $request->session()->flash('errors','Este usuario, no cuenta con el acceso necesario');
            return to_route('index');
        }else{
            return $next($request);
        }
    }
}
