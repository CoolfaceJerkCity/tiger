<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asistencia extends Model
{
    use HasFactory;

    protected $table = "asistencias";

    public function practicante(){
        return $this->belongsTo('App\Models\Practicante');
    }

    public function justificacion(){
        return $this->hasOne('App\Models\Justificacion');
    }
}
