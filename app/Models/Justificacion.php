<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Justificacion extends Model
{
    use HasFactory;

    protected $table = "justificaciones";

    public function asistencia(){
        return $this->belongsTo('App\Models\Asistencia');
    }

    public function archivos(){
        return $this->hasMany('App\Models\Archivo');
    }
}
