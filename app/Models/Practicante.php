<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Practicante extends Model
{
    use HasFactory;

    protected $table = "practicantes";

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function turno(){
        return $this->belongsTo('App\Models\Turno');
    }

    public function asistencias(){
        return $this->hasMany('App\Models\Asistencia');
    }

    public function fullname(){
        return sprintf("%s %s, %s",$this->apePaterno, $this->apeMaterno, $this->nombres);
    }
}
