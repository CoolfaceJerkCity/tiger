<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Turno extends Model
{
    use HasFactory;

    protected $table = "turnos";

    public function semestre(){
        return $this->belongsTo('App\Models\Semestre');
    }

    public function practicantes(){
        return $this->hasMany('App\Models\Practicante');
    }
}
