<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('practicantes', function (Blueprint $table) {
            $table->id();
            $table->string('dni', 8)->unique();
            $table->string('apePaterno', 100);
            $table->string('apeMaterno', 100);
            $table->string('nombres', 100);
            $table->date('fecNacimiento')->nullable();
            $table->string('sexo', 1)->nullable();
            $table->string('diasDescanso', 2)->nullable();
            $table->unsignedBigInteger('user_id')->unique();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->unsignedBigInteger('turno_id')->nullable();
            $table->foreign('turno_id')
                ->references('id')
                ->on('turnos')
                ->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('practicantes');
    }
};
