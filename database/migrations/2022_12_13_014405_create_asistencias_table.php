<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asistencias', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('practicante_id');
            $table->foreign('practicante_id')
                ->references('id')
                ->on('practicantes')
                ->onDelete('cascade');

            $table->date('fecha');
            $table->time('horaEnt')->nullable();
            $table->time('horaSal')->nullable();
            $table->tinyInteger('estado')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asistencias');
    }
};
