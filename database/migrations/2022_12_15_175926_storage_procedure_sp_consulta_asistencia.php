<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `sp_consulta_asistencia` (IN _prac INT)
        BEGIN
            SET @contador=0;
            SET lc_time_names = 'es_PE';
            SELECT @contador:=@contador+1 AS `nro`, DATE_FORMAT(`fecha`, '%W %e/%c/%Y') as fecha,
            TIME_FORMAT(`horaEnt`, '%r') AS horaEnt, TIME_FORMAT(`horaSal`, '%r') AS horaSal, `estado`,
            `id` FROM `asistencias` WHERE `practicante_id`=_prac ORDER BY `fecha` DESC;
        END
        ";

        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $procedure = "DROP PROCEDURE IF EXISTS sp_consulta_asistencia;";
        DB::unprepared($procedure);
    }
};
