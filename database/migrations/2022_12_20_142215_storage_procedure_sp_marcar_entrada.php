<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = '
        CREATE PROCEDURE sp_marcar_entrada(IN _prac INT)
        BEGIN
            DECLARE hoy char(1);
            DECLARE diasAct, diasEst VARCHAR(6);
            DECLARE horaEntrada, horaSalida TIME;
            DECLARE horaMinima, horaMaxima TIME;
            DECLARE estado TINYINT(1);

            SELECT t.diasTurno, substring_index(t.horasTurno,"-", 1), substring_index(t.horasTurno,"-", -1),
            p.diasDescanso INTO diasAct, horaEntrada, horaSalida, diasEst FROM turnos as t
            inner join practicantes as p ON p.turno_id = t.id WHERE p.id = _prac;

            SET horaMinima:= DATE_SUB(time(horaEntrada), INTERVAL 15 MINUTE);
            SET horaMaxima:= DATE_ADD(time(horaEntrada), INTERVAL 5 MINUTE);
            SET hoy :=CONCAT(ELT(WEEKDAY(curdate()) + 1, "L","M","X","J","V","S","D"));

            if(SELECT locate(hoy, diasEst)) then
                SELECT "No puedes marcar tu asistencia en un dia de descanso." as "text", "warning" as "type";

            elseif(SELECT locate(hoy, diasAct) = 0) then
                SELECT "No puedes marcar tu asistencia en un dia fuera de su horario." as "text", "warning" as "type";

            elseif((SELECT count(*) FROM asistencias WHERE practicante_id=_prac AND fecha = curdate()) = 1) then
                SELECT "Ya marcaste tu entrada para el dia de hoy" as "text", "warning" as "type";

            else
                if(curtime() > horaMinima AND curtime() < horaMaxima) then
                    set estado = 1;

                elseif(curtime() > horaMaxima AND curtime() < horaSalida) then
                    set estado = 2;

            end if;

            if(SELECT ISNULL(estado)) then
                SELECT "No puedes marcar asistencia fuera de tu horario" as "text", "error" as "type";
            else
                INSERT INTO asistencias(practicante_id, fecha, horaEnt, estado)
                VALUES(_prac, curdate(), curtime(), estado);

                SELECT "Marcaste tu entrada exitosamente" as "text", "success" as "type";
                end if;
            end if;
        END
        ';

        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $procedure = "DROP PROCEDURE IF EXISTS sp_marcar_entrada;";
        DB::unprepared($procedure);
    }
};
