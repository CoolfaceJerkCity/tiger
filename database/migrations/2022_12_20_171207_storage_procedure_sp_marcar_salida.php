<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
        CREATE PROCEDURE `sp_marcar_salida` (IN _prac INT)
        BEGIN
            DECLARE idAsistencia INT;
            DECLARE horaSalida, horaMinima TIME;

                /* OBTENIENDO HORA DE SALIDA DEL PRACTICANTE */
                SELECT substring_index(t.horasTurno,'-', -1) INTO
                horaSalida FROM turnos as t INNER JOIN practicantes as p
                ON p.turno_id = t.id WHERE p.id = _prac;

                /* OBTENIENDO LA HORA MINIMA DE SALIDA (10 minutos antes) */
                SET horaMinima= DATE_SUB(time(horaSalida), INTERVAL 10 MINUTE);

                /* OBTENIENDO LA ASISTENCIA DEL DIA DE HOY */
            SELECT id INTO idAsistencia FROM asistencias WHERE practicante_id = _prac AND fecha = curdate();

                /* VERIFICANDO QUE SE HAYA MARCADO ENTRADA PRIMERAMENTE */
            if(SELECT ISNULL(idAsistencia)) then
                     SELECT 'Debes de marcar tu entrada primeramente' as 'text', 'warning' as 'type';

              /* VERIFICANDO QUE NO HAYA MARCADO SALIDA ESE DIA */
                elseif((SELECT COUNT(*) FROM asistencias WHERE id=idAsistencia AND horaSal IS NULL)=0) then
                    SELECT 'Ya marcaste tu salida para el dia de hoy' as 'text', 'warning' as 'type';

                else
                    /* VERIFICAR HORA DE SALIDA */
                    if(curtime() < horaMinima) then
                        SELECT 'No puedes marcar tu salida, antes de tu hora mínima' as 'text', 'error' as 'type';
                    else
                        UPDATE asistencias SET horaSal= curtime() WHERE id = idAsistencia;
                        SELECT 'Marcaste tu salida exitosamente' as 'text', 'success' as 'type';
                    end if;
            end if;
        END
        ";

        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $procedure = "DROP PROCEDURE IF EXISTS sp_marcar_salida;";
        DB::unprepared($procedure);
    }
};
