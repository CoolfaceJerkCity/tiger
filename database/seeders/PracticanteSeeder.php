<?php

namespace Database\Seeders;

use App\Models\Practicante;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PracticanteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pra1 = new Practicante();
        $pra1->dni = "74119874";
        $pra1->apePaterno = "GARCÍA";
        $pra1->apeMaterno = "CHINGUEL";
        $pra1->nombres = "EMERSSON ALDHAIR";
        $pra1->fecNacimiento = "2003-05-23";
        $pra1->sexo = "M";
        $pra1->diasDescanso = "S";
        $pra1->user_id = 1;
        $pra1->turno_id = 1;
        $pra1->save();

        $pra2 = new Practicante();
        $pra2->dni = "12345678";
        $pra2->apePaterno = "CASTRO";
        $pra2->apeMaterno = "GUTIERREZ";
        $pra2->nombres = "VALERIA TILINA";
        $pra2->fecNacimiento = "2006-02-10";
        $pra2->sexo = "F";
        $pra2->diasDescanso = "X";
        $pra2->user_id = 2;
        $pra2->turno_id = 2;
        $pra2->save();
    }
}
