<?php

namespace Database\Seeders;

use App\Models\Semestre;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SemestreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $semestre1 = new Semestre();
        $semestre1->codigo ="20221";
        $semestre1->inicio ="2022-03-15 00:00:00";
        $semestre1->final ="2022-07-15 00:00:00";
        $semestre1->estado =0;
        $semestre1->save();

        $semestre2 = new Semestre();
        $semestre2->codigo ="20222";
        $semestre2->inicio ="2022-08-15 00:00:00";
        $semestre2->final ="2022-12-15 00:00:00";
        $semestre2->estado =1;
        $semestre2->save();
    }
}
