<?php

namespace Database\Seeders;

use App\Models\Turno;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TurnoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $turno_manana = new Turno();
        $turno_manana->codigo = "PRA-T1";
        $turno_manana->semestre_id = 2;
        $turno_manana->descripcion = "Turno mañana para practicantes";
        $turno_manana->diasTurno = "LMXJVS";
        $turno_manana->horasTurno = "08:00:00-14:00:00";
        $turno_manana->save();

        $turno_tarde = new Turno();
        $turno_tarde->codigo = "PRA-T2";
        $turno_tarde->semestre_id = 2;
        $turno_tarde->descripcion = "Turno tarde para practicantes";
        $turno_tarde->diasTurno = "LMXJVS";
        $turno_tarde->horasTurno = "14:00:00-20:00:00";
        $turno_tarde->save();
    }
}
