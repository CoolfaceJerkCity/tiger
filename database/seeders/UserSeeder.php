<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = new User();
        $user1->name = "74119874";
        $user1->email = "74119874@gmail.com";
        $user1->password = bcrypt("74119874");
        $user1->save();

        $user2 = new User();
        $user2->name = "12345678";
        $user2->email = "12345678@gmail.com";
        $user2->password = bcrypt("12345678");
        $user2->save();
    }
}
