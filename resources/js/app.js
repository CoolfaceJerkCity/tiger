/* Vite Kernel */
import './bootstrap';

/* Bootstrap 4.6.2 */
import '../css/app.scss';
import 'bootstrap';

/* Icons */
import 'font-awesome/css/font-awesome.css';
import 'font-awesome/fonts/FontAwesome.otf';

/* Custom Datatables */
import './custom-datatables';

/* Custom SweetAlert2 */
import './custom-sweetalerts';
