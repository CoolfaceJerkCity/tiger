//Implementar datatable en la tabla de asistencias
import jsZip from 'jszip';

window.JSZip = jsZip;

import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";

pdfMake.vfs = pdfFonts.pdfMake.vfs;

import 'datatables.net';
import 'datatables.net-buttons-bs4';
import 'datatables.net-buttons/js/buttons.flash';
import 'datatables.net-buttons/js/buttons.html5';
import 'datatables.net-responsive';

export const table_asistencia = $('#table_asistencia').DataTable({
    lengthMenu: [5, 10, 15, 20],
    destroy: true,
    autoWidth: false,
    deferRender: true,
    responsive: true,
    processing: true,
    dom: 'B<"row"<"col-12 col-md-6"l><"col-12 col-md-6"f>>rt<"row mt-3"<"col-12 col-md-6"i><"col-12 col-md-6"p>>',
    buttons: {
        buttons: [
            {extend: 'pdfHtml5',
                download: 'open',
                pageSize: 'LEGAL',
                text: '<i class="fa fa-file-pdf-o" aria-hidden="true"></i> Descargar Pdf',
                className: 'btn-danger'
            },
            {extend: 'excelHtml5',
                text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i> Descargar Excel',
                className: 'btn-success'
            },
            {extend: 'csvHtml5',
                text: '<i class="fa fa-file-text-o" aria-hidden="true"></i> Descargar Csv',
                className: 'btn-warning text-light',
                exportOptions: {
                    modifier: {
                        page: 'current'
                    }
                },
            },
            {text: '<i class="fa fa-refresh" aria-hidden="true"></i> Actualizar',
                className: 'btn-primary',
                action: function ( e, dt, node, config ) {
                    dt.ajax.reload();
                }
            }],
        dom: {
            button: { className: "btn m-1"},
            buttonLiner: { tag: null }
        }
    },
    ajax: {
        url: window.location.pathname,
        type: 'POST',
        data: {
            "_token": $('meta[name="csrf-token"]').attr('content'),
            "action": "list"
        },
    },
    columns: [
        {data: 'nro'},
        {data: 'fecha'},
        {data: 'horaEnt'},
        {data: 'horaSal'},
        {data: 'estado'},
    ],
    columnDefs: [
        {
            targets: [1],
            render: function (data, type, row) {
                return data[0].toUpperCase() + data.substring(1);
            }
        },
        {
            targets: [-2],
            class: 'text-center',
            orderable: false,
            render: function (data, type, row) {
                if(!data) return '<span class="font-weight-bold">Aun no sale</span>';
                return data;
            }
        },
        {
            targets: [-1],
            class: 'text-center font-weight-bold',
            orderable: false,
            render: function (data, type, row) {
                let innerState;
                switch (data) {
                    case null:
                        innerState = '<span class="text-secondary">Desconocido</span>';
                        break;
                    case 1:
                        innerState = '<span class="text-primary">Puntual</span>';
                        break;
                    case 2:
                        innerState = '<span class="text-secondary">Tardanza</span>';
                        innerState += `<button class="ml-2 btn btn-sm btn-primary btn-justificar" data-id="${row.id}"><i class="fa fa-book" aria-hidden="true"></i></button>`;
                        break;
                    case 0:
                        innerState = '<span class="text-danger">Ausente</span>';
                        break;
                }
                return innerState;
            }
        },
    ],
    language: {
        "lengthMenu": "Mostrar _MENU_ registros",
        "zeroRecords": "No se encontró ningún dato :(",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(Filtro aplicado en un total de _MAX_ registros)",
        "search": "Buscar",
        "searchPlaceholder": "Puntual...",
        "processing": "Cargando...",
    }
});

//Mostrar modal de justificacion
$('#table_asistencia tbody').on('click', 'button.btn-justificar', function () {
    alert('Justificando asistencia: ' + $(this).data("id"));
});
