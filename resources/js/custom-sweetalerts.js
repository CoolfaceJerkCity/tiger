import Swal from "sweetalert2";
import {table_asistencia} from "./custom-datatables";

let iconColor = getComputedStyle(document.documentElement).getPropertyValue('--primary');

/* Custom Swal Mixins */
const Toast = Swal.mixin({
    iconColor,
    toast: true,
    position: 'bottom-end',
    showConfirmButton: false,
    timer: 1500,
    timerProgressBar: true,
    didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
})
const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
        confirmButton: 'btn btn-primary mx-2',
        cancelButton: 'btn btn-secondary mx-2'
    },
    buttonsStyling: false,
})

/* Exportando globalmente Swal & Toast */
//window.Swal = Swal;
window.Toast = Toast;

/* Función para obtener los errores de mis peticiones AJAX */
function message_error(obj) {
    let html = '';
    if (typeof (obj) === 'object') {
        html = '<ul style="text-align: left;">';
        $.each(obj, function (key, value) {
            html += '<li>' + value + '</li>';
        });
        html += '</ul>';
    } else {
        html = '<p>' + obj + '</p>';
    }
    Swal.fire({
        title: 'Error!',
        html: html,
        icon: 'error'
    });
}

/* Función para enviar peticiones AJAX a mi servidor */
function submit_with_ajax({url, title, content, parameters, callback = null}) {
    /* Agregando el csrf token*/
    parameters.append("_token",$('meta[name="csrf-token"]').attr('content'));

    /* Ejecutando la confirmación de la petición */
    swalWithBootstrapButtons.fire({
        title: title,
        text: content,
        icon: "warning",
        iconColor,
        backdrop: true,
        showCancelButton: true,
        showLoaderOnConfirm: true,
        confirmButtonText: '<i class="fa fa-check-circle-o" aria-hidden="true"></i> Aceptar',
        cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Cancelar',
        allowOutsideClick: () => false,
        allowEscapeKey: () => false,
        preConfirm: () => {
            return new Promise(function (resolve) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: parameters,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                }).done(function (response) {
                    if (!response.hasOwnProperty('error')) {
                        let {type, text} = response.data[0];
                        Toast.fire({'icon': type, 'title': text});
                        if(type === "success") {
                            if(callback) callback();
                        }
                        return false;
                    }
                    message_error(response.error);

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    Toast.fire({
                        icon: 'error',
                        title: 'Ocurrio un error inesperado :(',
                        text: 'Lo estaremos arreglando pronto'
                    });
                    alert(textStatus + ' : ' + errorThrown);
                }).always(function (data) {

                });
            }).catch(Swal.noop)
        },
    });
}

/* Botones de marcar entrada y salida */
$('#btnMarcarEntrada').on('click', function(){
    const data = new FormData();
    data.append("action", "input");

    submit_with_ajax({url: window.location.pathname,
    title: 'Confirmación', content: '¿Está seguro de marcar su entrada?', parameters: data, callback: function () {
         table_asistencia.ajax.reload();
    }});
});

$('#btnMarcarSalida').on('click', function(){
    const data = new FormData();
    data.append("action", "output");

    submit_with_ajax({url: window.location.pathname,
        title: 'Confirmación', content: '¿Está seguro de marcar su salida?', parameters: data, callback: function () {
            table_asistencia.ajax.reload();
    }});
});
