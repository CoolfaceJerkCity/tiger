<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>UNI - {{$title}}</title>
    @vite(['resources/css/app.scss', 'resources/js/app.js'])
</head>
<body>
<!-- Navigation Component -->
<x-layouts.navigation />

<!-- Main Content -->
<main class="wrapper bg-light">
    <div class="container py-5 px-2">
        {{ $slot }}
    </div>
</main>

@if(session('status'))
    <script type="module">
        Toast.fire({'icon': 'success', 'title': "{{session('status')}}"});
    </script>
@endif

@if(session('errors'))
    <script type="module">
        Toast.fire({'icon': 'success', 'title': "{{session('errors')}}"});
    </script>
@endif

<!-- Custom Scripts -->
@stack('scripts')
</body>
</html>
