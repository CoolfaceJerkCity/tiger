<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand nav-head text-truncate" href="#">
        <i class="fa fa-user mr-1" aria-hidden="true"></i>
        @auth
            @if(Auth::user()->practicante)
                <span>{{ Auth::user()->practicante->fullname() }}</span>
            @else
                <span>{{ Auth::user()->name }}</span>
            @endif
        @endauth
        @guest
            <span>INVITADO</span>
        @endguest
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav ml-auto">
            @auth
                <a class="nav-link {{ request()->routeIs('index') ? 'active' : '' }}" href="{{ route('index') }}">Home</a>
                <a class="nav-link" href="#">Report</a>
                <a class="nav-link {{ request()->routeIs('assist') ? 'active' : '' }}" href="{{ route('assist') }}">Assist</a>
                <a class="nav-link {{ request()->routeIs('excuse') ? 'active' : '' }}" href="{{ route('excuse') }}">Excuse</a>
                <form action="{{route('logout')}}" method="POST">
                    @csrf
                    <a class="nav-link" href="#" onclick="this.closest('form').submit()">Logout</a>
                </form>
            @endauth
            @guest
                <a class="nav-link {{ request()->routeIs('login') ? 'active' : '' }}" href="{{ route('login') }}">Login</a>
            @endguest
        </div>
    </div>
</nav>
