<x-layouts.app title="Login">
    <div class="card login-card mx-auto">
        <div class="card-body">
            <h4 class="text-center">Formulario</h4>
            <form action="{{ route('login') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label>Usuario</label>
                    <input type="text" class="form-control" name="name" value="{{ old('name') }}" required/>
                    @error('name')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Contraseña</label>
                    <input type="password" class="form-control" name="password" required/>
                    @error('password')
                    <small class="form-text text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" name="remember"/>
                    <label class="form-check-label">Recuérdame</label>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-block btn-primary">INICIAR SESIÓN</button>
                </div>
            </form>
        </div>
    </div>
</x-layouts.app>
