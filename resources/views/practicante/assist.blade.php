<x-layouts.app title="Assist">
    <div class="card mb-3">
        <div class="card-body">
            <div class="container mt-3 px-0 text-center">
                <h3><i class="fa fa-pencil-square text-primary" aria-hidden="true"></i><b> Marcar asistencia</b></h3>
                <hr>
            </div>
            <div class="container mt-3 px-0 text-center">
                <div class="row">
                    <div class="col-12 mb-3">
                        <label><strong class="text-primary">Entrada: </strong>Se podra marcar entrada desde 15min antes de su hora de entrada.</label>
                        <label><strong class="text-primary">Tolerancia: </strong>Se tendra una tolerancia de 5min, pasado los 5min se registrará como tardanza.</label>
                        <label><strong class="text-primary">Salida: </strong>Se podra marcar salida desde 10min antes de su hora de salida.</label>
                    </div>
                    <div class="col-12 mb-3">
                        <button id="btnMarcarEntrada" class="btn btn-primary mx-2">
                            <i class="fa fa-sign-in" aria-hidden="true"></i>
                            Entrada
                        </button>
                        <button id="btnMarcarSalida" class="btn btn-secondary mx-2">
                            <i class="fa fa-sign-out" aria-hidden="true"></i>
                            Salida
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="container mt-3 px-0 text-center">
                <h3><i class="fa fa-history text-primary" aria-hidden="true"></i><b> Historial de asistencias</b></h3>
                <hr>
            </div>
            <div class="container mt-3 px-0">
                <table id="table_asistencia" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                    <tr>
                        <th class="text-center">N°</th>
                        <th class="text-center">Fecha</th>
                        <th class="text-center">Entrada</th>
                        <th class="text-center">Salida</th>
                        <th class="text-center">Estado</th>
                    </tr>
                    </thead>
                    <tbody class="text-center">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</x-layouts.app>
