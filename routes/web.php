<?php

use App\Http\Controllers\AsistenciaController;
use App\Http\Controllers\Auth\SessionUserController;
use App\Http\Controllers\PracticanteController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/','index')->middleware('auth')->name('index');
Route::view('/login','login')->middleware('guest')->name('login');
Route::post('/login', [SessionUserController::class, 'store']);
Route::post('/logout', [SessionUserController::class, 'destroy'])->name('logout');

Route::get('/assist',[PracticanteController::class, 'assist'])->name('assist');
Route::get('/excuse',[PracticanteController::class, 'excuse'])->name('excuse');

/*AJAX Methods*/
Route::post('/assist',[AsistenciaController::class, 'postRequest']);
